import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Loading from './common/components/loading/Loading';

const HCardBuilderPage = lazy(() => import('./pages/hCardBuilder/HCardBuilderPage'));

function Routes() {
  return (
    <Router>
      <Suspense fallback={<Loading />}>
        <Switch>
          <Route exact path="/" component={HCardBuilderPage} />
        </Switch>
      </Suspense>
    </Router>
  );
}

export default Routes;
