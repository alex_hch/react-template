/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';

import * as hooks from './hooks';

function SaveButton({
  label,
  id,
  onClick,
}) {
  const classes = hooks.useStyles();

  return (
    <React.Fragment>
      <label htmlFor={id}>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={onClick}
        >
          {label}
        </Button>
      </label>
    </React.Fragment>
  );
}

SaveButton.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default SaveButton;
