import { makeStyles } from '@material-ui/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles(theme => ({
  input: {
    display: 'none',
  },
  button: {
    margin: theme.spacing.unit,
    width: 190,
    height: 40,
    borderRadius: 4,
    color: theme.palette.common.white,
    backgroundColor: '#3f515d',
    textTransform: 'none',
    fontSize: 17,
    fontWeight: 400,
  },
}));
