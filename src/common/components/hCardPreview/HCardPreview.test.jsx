import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import { stub } from 'sinon';

import Typography from '@material-ui/core/Typography';

import HCardPreview from './HCardPreview';
import HCard from '../hCard/HCard';
import * as hooks from './hooks';

function setup(specProps) {
  const defaultProps = {};

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<HCardPreview {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('HCardPreview', () => {
  before(() => {
    stub(hooks, 'useStyles').returns({});
  });

  after(() => {
    hooks.useStyles.restore();
  });

  it('should render self and have first div', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is('div')).to.equal(true);
  });

  it('should render one Typography', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(Typography)).length(1);
  });

  it('should render one HCard', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find(HCard)).length(1);
  });
});
