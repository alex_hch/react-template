import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';

import HCard from '../hCard/HCard';

import * as hooks from './hooks';

function HCardPreview({
  name,
  surname,
  email,
  phone,
  house,
  street,
  suburb,
  state,
  postcode,
  country,
  avatar,
}) {
  const classes = hooks.useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="subtitle1" align="right">
      HCard Preview
      </Typography>
      <HCard
        name={name}
        surname={surname}
        email={email}
        phone={phone}
        house={house}
        street={street}
        suburb={suburb}
        state={state}
        postcode={postcode}
        country={country}
        avatar={avatar}
      />
    </div>
  );
}

HCardPreview.propTypes = {
  name: PropTypes.string.isRequired,
  surname: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  house: PropTypes.string.isRequired,
  street: PropTypes.string.isRequired,
  suburb: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  postcode: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
};

export default HCardPreview;
