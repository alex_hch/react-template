import { makeStyles } from '@material-ui/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
}));
