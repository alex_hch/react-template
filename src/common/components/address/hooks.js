import { makeStyles } from '@material-ui/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  divider: {
    width: '100%',
    marginBottom: 16,
    marginTop: 3,
  },
}));
