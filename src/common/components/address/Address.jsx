import React from 'react';
import PropTypes from 'prop-types';

import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

import TextInput from '../textInput/TextInput';

import * as hooks from './hooks';

function Address({
  house,
  street,
  suburb,
  state,
  postcode,
  country,
  onChange,
}) {
  const classes = hooks.useStyles();

  return (
    <div className={classes.root}>
      <Typography variant="h2">
          ADDRESS
      </Typography>
      <Divider className={classes.divider} />
      <TextInput
        id="house-input"
        label="HOUSE NAME OR #"
        value={house}
        onChange={onChange}
      />
      <TextInput
        id="street-input"
        label="STREET"
        value={street}
        onChange={onChange}
      />
      <TextInput
        id="suburb-input"
        label="SUBURB"
        value={suburb}
        onChange={onChange}
      />
      <TextInput
        id="state-input"
        label="STATE"
        value={state}
        onChange={onChange}
      />
      <TextInput
        id="postcode-input"
        label="POSTCODE"
        value={postcode}
        onChange={onChange}
      />
      <TextInput
        id="country-input"
        label="COUNTRY"
        value={country}
        onChange={onChange}
      />
    </div>
  );
}

Address.propTypes = {
  onChange: PropTypes.func.isRequired,
  house: PropTypes.string.isRequired,
  street: PropTypes.string.isRequired,
  suburb: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  postcode: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
};

export default Address;
