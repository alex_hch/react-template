import React from 'react';
import PropTypes from 'prop-types';

import './hCard.scss';

function HCard({
  name,
  surname,
  email,
  phone,
  house,
  street,
  suburb,
  state,
  postcode,
  country,
  avatar,
}) {
  return (
    <div id={`hcard-${name}-${surname}`} className="vcard">
      <div className="fn">
        {`${name} ${surname}`}
      </div>
      <img className="photo" src={avatar} alt={`${name} ${surname}`} />
      <div className="items">
        <div className="item">
          <span className="label">Email</span>
          <span className="email">{email}</span>
        </div>
        <div className="item">
          <span className="label">Phone</span>
          <span className="tel">{phone}</span>
        </div>
        <div className="adr">
          <div className="item">
            <span className="label">Address</span>
            <span className="extended-address">{house}</span>
            <span className="street-address">{street}</span>
          </div>
          <div className="item">
            <span className="label" />
            <span className="locality">{suburb}</span>
            {
              suburb && state && ','
            }
            <span className="region">{state}</span>
          </div>
          <div className="item">
            <div className="item-half">
              <span className="label">Postcode</span>
              <span className="postal-code">{postcode}</span>
            </div>
            <div className="item-half">
              <span className="label">Country</span>
              <span className="country-name">{country}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

HCard.propTypes = {
  name: PropTypes.string.isRequired,
  surname: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  house: PropTypes.string.isRequired,
  street: PropTypes.string.isRequired,
  suburb: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  postcode: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
};

export default HCard;
