import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import HCard from './HCard';

function setup(specProps) {
  const defaultProps = {};

  const props = {
    ...defaultProps,
    ...specProps,
  };
  const enzymeWrapper = shallow(<HCard {...props} />);

  return {
    props,
    enzymeWrapper,
  };
}

describe('HCard', () => {
  it('should render self and have first div', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.first().is('div')).to.equal(true);
  });

  it('should render eleven divs', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find('div')).length(11);
  });

  it('should render fourteen spans', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find('span')).length(14);
  });

  it('should render one img', () => {
    const { enzymeWrapper } = setup();
    expect(enzymeWrapper.find('img')).length(1);
  });
});
