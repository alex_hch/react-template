# hCard Builder

## Start application

- `npm i`
- `npm start`

## Run EsLint check

- `npm run lint`

## Run unit tests

- `npm test`

## Check test coverage

- `npm run test:cov`

## Run build

- `npm run build`